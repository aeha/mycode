import requests
import folium

# Retrieve ISS location data
response = requests.get("http://api.open-notify.org/iss-now.json")
data = response.json()

latitude = float(data['iss_position']['latitude'])
longitude = float(data['iss_position']['longitude'])

# Create a folium map centered around the ISS location
map_iss = folium.Map(location=[latitude, longitude], zoom_start=4)

# Add a marker for the ISS location
folium.Marker(location=[latitude, longitude], icon=folium.Icon(icon='cloud')).add_to(map_iss)

# Display the map
map_iss.save('iss_map.html')

