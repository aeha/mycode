#!/usr/bin/env python3
import re
# standard library
import os

# 3rd party imports
# python3 -m pip install requests
import requests     # used to make HTTP requests (GET, POST, PUT, DELETE, etc.)

# this a GLOBAL CONSTANT (this is why it is all capitalized)
API = "http://api.open-notify.org/astros.json"

def main():
    """run time code"""

    # from now on all commands are relative to this location
    os.chdir("/home/student/mycode")

    # perform an HTTP GET request
    response = requests.get(API)   # response is an HTTP response object
    response = response.json()
    #print(response.status_code)   # returns the HTTP status code returned to us

    listofdicts= response["people"]
    for astrodict in listofdicts:
        print(astrodict["name"])
        print(astrodict["craft"])

    # strip JSON off of response
    #space_data = response.json()   # all requests objects, have a .json() method
     
    # this strips JSON off the 200+JSON and converts it
                                   # to pythonic data types WITHOUT the need to
                                   # "import json" from the standard library

    # test to ensure I can now work with the data in Python
    #print(space_data)              # we should now see the data from the file
    #print(type(space_data))        # the data type should be 'dict' NOT 'str'
    #print(space_data.get('id'))    # perform a test lookup on a 'dict' data type

# best practice to call main()
if __name__ == "__main__":
    main()
